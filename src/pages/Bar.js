import { AiOutlineUsergroupAdd } from 'react-icons/ai'
import { FaCalendarAlt, FaRegClock } from 'react-icons/fa'
import HeaderShowcase from '../components/HeaderShowcase'
import styles from './styles/Bar.module.css'

const BarPage = () => {
  return (
    <>
      {/* == Header Showcase == */}
      <HeaderShowcase />
      {/* == Table Booking Section == */}
      <div className={styles.sectionBookingForm}>
        <h3>BOOK YOUR SEATS NOW</h3>
        <form>
          <div className={styles.sectionBookingForm__formGroup}>
            <select>
              <option value='1'>1 Person</option>
              <option value='2'>2 Person</option>
              <option value='3'>3 Person</option>
              <option value='4'>4 Person</option>
              <option value='5'>5 Person</option>
              <option value='6'>6 Person</option>
              <option value='7'>7 Person</option>
              <option value='8'>8 Person</option>
              <option value='9'>9 Person</option>
              <option value='10'>10 Person</option>
            </select>
            <div>
              <AiOutlineUsergroupAdd size={30} />
            </div>
          </div>
          <h4>FOR</h4>
          <div className={styles.sectionBookingForm__formGroup}>
            <input type='date' />
            <div>
              <FaCalendarAlt size={30} />
            </div>
          </div>
          <h4>AT</h4>
          <div className={styles.sectionBookingForm__formGroup}>
            <select>
              <option value='6:30am'>6:30am</option>
              <option value='7:00am'>7:00am</option>
              <option value='7:30am'>7:30am</option>
              <option value='8:00am'>8:00am</option>
              <option value='8:30am'>8:30am</option>
              <option value='9:00am'>9:00am</option>
              <option value='9:30am'>9:30am</option>
              <option value='10:00am'>10:00am</option>
              <option value='10:30am'>10:30am</option>
              <option value='11:00am'>11:00am</option>
            </select>
            <div>
              <FaRegClock size={30} />
            </div>
          </div>
          <input
            type='submit'
            value='BOOK'
            className={styles.sectionBookingForm__btn}
          />
        </form>
      </div>
      {/* == About The Bar Section == */}
      <section className={styles.sectionAbout}>
        <div className={styles.sectionAbout__content}>
          <div className={styles.sectionAbout__contentImages}>
            <div className={styles.sectionAbout__contentImages_img1} />
            <div className={styles.sectionAbout__contentImages_img2} />
            <div className={styles.sectionAbout__contentImages_img3} />
            <div className={styles.sectionAbout__contentImages_img4} />
          </div>
          <div className={styles.sectionAbout__contentText}>
            <h2>ABOUT US</h2>
            <p>A little about us and a breif history of how we started.</p>
            <p>
              Cras ut viverra eros. Phasellus sollicitudin sapien id luctus
              tempor. Sed hend rerit inter dum sagittis. Donec nunc lacus,
              dapibus nec interdum eget, ultrices eget justo. Nam purus lacus,
              efficitur eget laoreet sed, finibus nec neque. Cras eget enim in
              diam dapibus sagittis. In massa est, dignissim in libero ac,
              fringilla ornare mi. Etiam interdum ligula purus.
            </p>
            <p>
              Ultrices eget justo. Nam purus lacus, efficitur eget laoreet sed,
              finibus nec neque. Cras eget enim in diam dapibus sagittis. In
              massa est, dignissim in libero ac, fringilla ornare.
            </p>
          </div>
        </div>
      </section>
      {/* == Menu Section == */}
      <section className={styles.sectionMenu}>
        <p>Our finest</p>
        <h2>CRAFT BEER SELECTION</h2>
        <div className={styles.sectionMenu__menuGrid}>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-summit-craft.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-weisse.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-barley-garden.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-bomber.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-brooklyn.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-hillbrothers.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-hobgoblin.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-paulaner.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-bluemoon.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-cultre-ontrap.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-brewdog.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
          <div className={styles.sectionMenu__item}>
            <img src='/static/menu-knocker.png' alt='' />
            <div className='flex justifyContent-spaceBetween alignItems-center'>
              <h2>SUMMIT CRAFT</h2>
              <h2>$2.95</h2>
            </div>
          </div>
        </div>
      </section>
      {/* == Reservations Section == */}
      <section className={styles.sectionReservations}></section>
      {/* == Testimonials Section == */}
      <section className={styles.sectionTestimonials}></section>
    </>
  )
}
export default BarPage
